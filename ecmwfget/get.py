# FIXME: move number of model levels from parameters to model (or, rather, make a combined thing as with noparam/param)
import copy
import datetime
from itertools import product
import random
from time import sleep
import yaml

from ecmwfapi import ECMWFDataServer, ECMWFService
from joblib import Parallel, delayed
import pandas as pd


def load_config(path, key):
    with open(path, 'r') as fd:
        fullcfg = yaml.load(fd)
    cfg = fullcfg.get(key)
    if cfg is None:
        raise ValueError(
            "Cannot find configuration for '{}' in '{}'.format(key, path)")
    return cfg


def construct_requests(day, config, target, delta_t=None, static_fields=False, addopts=None):
    """Construct all MARS requests for one day

    Parameters
    ----------
    day : datetime.date
        The day for which data shall be requested

    config : dict

    target : str
        Template for target filename(s), including path component.
        The final filename will be constructed by calling the str's
        ``.format()`` method with the single request's config as
        parameter.

    delta_t : , optional
        The timespan to request data for per request.  Defaults to
        '1D' for one day.

    static_fields : bool, optional
        If *True*, download also those fields marked as ``static`` in
        the configuration.  Defults to *False*.

    addopts : dict, optional

        If provided, the key/value pairs from this dictionary will be
        added to all requests for this day.  Defaults to *None*, i.e.,
        no added key/value pairs.

        TODO: Why is this needed?

    Returns
    -------
    requests : dict
        A dictionary with target filenames as *keys* and single MARS
        request dictionarys as *values*.

    """
    # TODO add expect keyword to request
    # TODO implement delta_t
    if delta_t is None:
        try:
            delta_t = config['Metadata']['delta_t']
        except KeyError:
            pass
    if delta_t is not None:
        if delta_t not in ['1H']:
            raise ValueError(
                "Currently, only delta_t=1H is supported")
        delta_t = pd.Timedelta(delta_t)

    #if delta_t is not None:
    #    raise NotImplementedError(
    #        "Specifying 'delta_t' is not implemented yet")

    base = copy.deepcopy(config['Request'])
    times = config['Times']
    data = config['Parameters']

    def _get_actualtime(time, step):
        actualtime = datetime.timedelta(hours=int(tt.split(':')[0]))
        actualtime += datetime.timedelta(hours=int(ss))
        return int(actualtime.total_seconds() / 3600 % 24)
    
    # TODO pre-process times to reflect delta_t
    if delta_t == pd.Timedelta('1H'):
        times_new = []
        for t in times:
            for tt, ss in product(t['time'], t.get('step', [0])):
                t_new = copy.deepcopy(t)
                t_new['time'] = tt
                t_new['step'] = ss
                t_new['actualtime'] = _get_actualtime(tt, ss)
                times_new.append(t_new)
        times = times_new

    requests = {}
    for dd, tt in product(data, times):
        req = copy.deepcopy(base)
        dd = copy.deepcopy(dd)
        tt = copy.deepcopy(tt)
        if tt.get('noparam'):
            # TODO filter out unwanted parameters from dd
            noparam_ = tt.pop('noparam')
            dd['param'] = [p_ for p_ in dd['param'] if p_ not in noparam_]
        elif tt.get('param'):
            # TODO put only wanted parameters in dd
            param_ = tt.pop('param')
            dd['param'] = [p_ for p_ in dd['param'] if p_ in param_]

        req.update(dd)
        req.update(tt)
        # FIXME
        req['levelist'] = base['levelist']

        # add date for target filename purposes only
        req['date'] = day

        # construct target filename
        target_fn = target.format(**req)
        #req.pop('id')
        try:
            req.pop('actualtime')
        except KeyError:
            pass

        # overwrite date with the actual model date for the request
        model_day = copy.copy(day)
        if tt.get('date_offset'):
            model_day += datetime.timedelta(tt.get('date_offset'))
        req['date'] = '{:%Y-%m-%d}'.format(model_day)
        req.pop('date_offset', None)

        # write lists in MARS syntax, i.e., separated by '/'
        for k, v in req.items():
            if isinstance(v, list):
                req[k] = '/'.join([str(item) for item in v])

        if req.get('static') and not static_fields:
            # only make static request if explicitly requested
            continue
        elif req.get('static'):
            # only create one request for static fields
            static_fields = False
        else:
            req.pop('static', None)

        if req['param']:
            # sometimes (e.g., ERA5/ml) it can happen that for a given
            # parameters/type combination there are actually no
            # parameters to be retrieved.  therefore we have to check
            # in order to avoid MARS problems
            requests[target_fn] = req

    return requests


def execute(target, request, mars=True, auth=None, wait=True):
    """Execute one single MARS request"""
    if wait:
        sleep(random.randint(1, 10))
    if not auth:
        auth = {}
    if mars:
        E = ECMWFService('mars', **auth)
        E.execute(request, target)
    else:
        E = ECMWFDataServer(**auth)
        request['target'] = target
        E.retrieve(request)
    del E


def download(config, start, target, end=None, grid=None, area=None, static=False, n_jobs=1):
    """Download all data for given time range

    Parameters
    ----------
    config : dict

    start : datetime.datetime

    target : str

    end : datetime.datetime, optional

    grid : str or list, optional

    area : str or list, optional

    static : bool, optional

    n_jobs : int, optional

    """
    if grid or area:
        raise NotImplementedError

    now = copy.copy(start)
    if end is None:
        end = start

    requests = {}
    now = copy.copy(start)
    while now <= end:
        reqs = construct_requests(now, config, target, delta_t=None) #, static_fields=static, addopts={'grid': '0.1/0.1', 'area': '60/-5/40/40'})
        static = False
        requests.update(reqs)
        now += datetime.timedelta(1)

    targets = list(requests.keys())
    reqs = [requests[k] for k in targets]
    mars = config['mars']
    auth = config.get('auth')
    wait = True

    Parallel(n_jobs=n_jobs, pre_dispatch='1*n_jobs')(
             delayed(execute)(fn, req, mars, auth, wait) for fn, req in zip(targets, reqs))

    with open('ecmwfget_requests.txt', 'w') as fd:
        fd.write(yaml.dump(requests))
