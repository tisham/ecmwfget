import yaml


def load_config(paths, rcfile):
    models = {}
    params = {}
    for p in paths:
        with open(p, 'r') as fd:
            cfg = yaml.load(fd)
        if 'Model' in cfg:
            for k in cfg.get('Model'):
                if k in models:
                    raise ValueError('ALREADY PRESENT: ' + k)
                else:
                    models[k] = cfg['Model'][k]
        if 'Parameters' in cfg:
            for k in cfg.get('Parameters'):
                if k in params:
                    raise ValueError('ALREADY PRESENT: ' + k)
                else:
                    params[k] = cfg['Parameters'][k]

    if len(models) < 1:
        raise ValueError('No models')
    elif len(models) > 1:
        raise ValueError('Too many models')
    if len(params) < 1:
        raise ValueError('No parameter sets')

    cfg = models.values()[0]
    cfg.update({'Parameters': aggregate_params(params)})

    if rcfile:
        with open(rcfile, 'r') as fd:
            cfg['auth'] = yaml.load(fd.read())

    return cfg


def aggregate_params(cfg):
    """Aggregate parameter sets in case of several different parameter sets in one request"""
    cfg_agg = {}

    for params in cfg.values():
        for paramset in params:
            ix_ = paramset.get('id'), paramset.get('levtype'), paramset.get('levelist')
            #ix_ = paramset['id']
            #if paramset['id'] not in cfg_agg:
            if ix_ not in cfg_agg:
                cfg_agg[ix_] = {}
                cfg_agg[ix_]['param'] = []
                cfg_agg[ix_]['levtype'] = None

            cfg_agg[ix_]['param'] += paramset['param']
            cfg_agg[ix_]['param'] = list(set(cfg_agg[ix_]['param']))
            if cfg_agg[ix_]['levtype'] is not None:
                if cfg_agg[ix_]['levtype'] != paramset['levtype']:
                    raise ValueError()
            else:
                cfg_agg[ix_]['levtype'] = paramset['levtype']
            if 'levelist' in paramset and 'levelist' not in cfg_agg[ix_]:
                cfg_agg[ix_]['levelist'] = paramset['levelist']
            elif 'levelist' in paramset:
                if cfg_agg[ix_]['levelist'] != paramset['levelist']:
                    raise ValueError()
            else:
                try:
                    cfg_agg[ix_]['levelist'] = paramset['levelist']
                except KeyError:
                    if paramset['levtype'] != 'sfc':
                        raise ValueError()
    return list(cfg_agg.values())
