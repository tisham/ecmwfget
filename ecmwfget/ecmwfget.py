from __future__ import absolute_import

import argparse
import datetime
import yaml

from config import load_config
from get import download


def valid_date(s):
    # from https://stackoverflow.com/a/25470943/152439
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


# FIXME Why doesn't this lead to default values being displayed at `-h`?
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

subparsers = parser.add_subparsers(dest='action')

download_parser = subparsers.add_parser('download')
download_parser.add_argument(
    '-c', '--config', action='append',
    help='Path to .yaml configuration file(s) for ecmwfget.')
download_parser.add_argument(
    '-s', '--start', type=valid_date, required=True,
    help='First day to download data for [YYYY-MM-DD]')
download_parser.add_argument(
    '-e', '--end', type=valid_date, required=False,
    help='Last day to download data for [YYYY-MM-DD]')
download_parser.add_argument(
    '-t', '--target', type=str, required=True,
    help='Path where downloaded data shall be stored')
download_parser.add_argument(
    '-n', '--nproc', type=int, required=False, default=1,
    help='How many parallel MARS requests shall be executed?')
download_parser.add_argument(
    '--netcdf', required=False, default=False,
    action='store_true',
    help='Request data as netCDF files (default: Grib)')
download_parser.add_argument(
    '-m', '--mars', required=False, default=False,
    action='store_true', help='Request data from MARS')
download_parser.add_argument(
    '-r', '--rc',
    help='Path to .ecmwfapirc file (needed for authentication).')


def do_download(args):
    cfg = load_config(args.get('config'), args.get('rc'))
    cfg['mars'] = args['mars']
    download(cfg, args.get('start'), args.get('target'), end=args.get('end'), grid=None, area=None,
             n_jobs=args.get('nproc'))

    
if __name__ == '__main__':
    args = vars(parser.parse_args())
    with open('ecmwfget_args.txt', 'w') as fd:
        fd.write(yaml.dump(args))
    print(args)
    if args.get('action') == 'download':
        do_download(args)
