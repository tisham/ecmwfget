=============================================
ecmwfget -- Download ECMWF data, conveniently
=============================================

ecmwfget is a Python_ script to facilitate convenient downloading of
meteorological model data from ECMWF_.

.. _Python: https://www.python.org/
.. _ECMWF: https://www.ecmwf.int/

